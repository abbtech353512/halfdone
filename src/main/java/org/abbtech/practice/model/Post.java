package org.abbtech.practice.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "post")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String title;

    @Column(nullable = false)
    @Getter
    @Setter
    private String content;

    @Column(name = "created_date", nullable = false)
    @Getter
    @Setter
    private Date createdDate;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    @Getter
    private User user;
}
