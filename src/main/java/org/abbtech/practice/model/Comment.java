package org.abbtech.practice.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.dialect.InnoDBStorageEngine;
import org.yaml.snakeyaml.events.Event;

import java.util.Date;

@Entity
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String content;

    @Column(nullable = false)
    @Getter
    @Setter
    private Date createdDate;

    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    @Getter
    private Post post;
}
