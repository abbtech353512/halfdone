package org.abbtech.practice.bean;

import org.springframework.stereotype.Component;

@Component
public class SmsNotificationService implements NotificationService {
    @Override
    public void sendNotification() {
        System.out.println("send sms notification");
    }
}
