package org.abbtech.practice.bean;

public interface NotificationService {

    void sendNotification();
}
