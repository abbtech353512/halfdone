package org.abbtech.practice.dto;

import java.util.Date;

public record CommentReadDTO(
        Long id,
        String content,
        Date createDate,
        PostReadDTO post
) {
}
