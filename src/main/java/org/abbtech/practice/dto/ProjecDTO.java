package org.abbtech.practice.dto;

import jakarta.validation.constraints.NotNull;

public record ProjecDTO(@NotNull String name) {
}
