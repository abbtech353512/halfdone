package org.abbtech.practice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record CommentCreateDTO(
        @NotNull
        @NotBlank(message = "Comment content cannot be blank.")
        @Size(min = 5, max = 100)
        String content
) {
}
