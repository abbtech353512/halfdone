package org.abbtech.practice.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.constraints.Size;
import java.util.List;

public record EmployeeDTO(@NotBlank @Pattern(regexp = "name", message = "not matched with regex",groups = XUserGroup.class) String name,
                          @NotNull(message = "Not null message") @NotBlank(groups = XUserGroup.class) String department,
                          @Valid @Size(min = 1, max = 10, message = "limit message",groups = YUserGroup.class) List<ProjecDTO> projects,
                          @Positive @PositiveOrZero int age
) {
}
