package org.abbtech.practice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.Date;

public record PostCreateDTO(
        @NotNull
        @NotBlank(message = "Post title cannot be blank.")
        @Size(min = 3, max = 20)
        String title,

        @NotNull
        @NotBlank(message = "Post content cannot be blank.")
        @Size(min = 30, max = 150)
        String content
) {}
