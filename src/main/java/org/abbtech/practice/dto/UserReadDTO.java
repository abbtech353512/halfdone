package org.abbtech.practice.dto;

public record UserReadDTO(
        Long id,
        String username,
        String email
) {
}
