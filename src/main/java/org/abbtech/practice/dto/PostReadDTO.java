package org.abbtech.practice.dto;

import java.util.Date;

public record PostReadDTO(
        Long id,
        String content,
        String title,
        Date createDate,
        UserReadDTO user
) {
}
