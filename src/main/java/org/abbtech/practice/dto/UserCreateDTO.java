package org.abbtech.practice.dto;

import jakarta.validation.constraints.*;

public record UserCreateDTO(
    @NotNull
    @NotBlank(message = "Username cannot be empty.")
    @Size(min = 7, max = 15)
    String username,

    @NotNull
    @NotBlank(message = "Email cannot be empty.")
    @Email(message = "Email is in invalid format.")
    String email,

    @NotNull
    @NotBlank
    @Size(min = 10, max = 15)
    @Pattern(regexp = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$")
    String password
) {
}

