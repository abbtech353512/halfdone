package org.abbtech.practice;

import org.abbtech.practice.bean.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Arrays;

@SpringBootApplication
public class PracticeApplication {
    public static void main(String[] args) {
        var context = SpringApplication.run(PracticeApplication.class, args);
    }
}
