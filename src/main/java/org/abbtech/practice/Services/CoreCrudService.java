package org.abbtech.practice.Services;

import java.util.List;
import java.util.Optional;

public interface CoreCrudService<TEntity, TReadDto, TCreateDto> {
    void create(TEntity entity);
    TReadDto get(Long entityId);
    List<TReadDto> getAll();
    void update(Long entityId, TCreateDto newEntity);
    void delete(Long entityId);
}
