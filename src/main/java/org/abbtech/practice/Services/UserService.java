package org.abbtech.practice.Services;

import org.abbtech.practice.Repositories.CoreRepository;
import org.abbtech.practice.dto.UserCreateDTO;
import org.abbtech.practice.dto.UserReadDTO;
import org.abbtech.practice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements CoreCrudService<User, UserReadDTO, UserCreateDTO> {

    private final CoreRepository<User> repository;

    @Autowired
    public UserService(CoreRepository<User> repository) {
        this.repository = repository;
    }

    @Override
    public void create(User user) {
        repository.save(user);
    }

    @Override
    public UserReadDTO get(Long entityId) {
        var optionalEntity = repository.findById(entityId);

        if (optionalEntity.isEmpty()) {
            return null;
        }

        var entity = optionalEntity.get();
        return new UserReadDTO(
                entity.getId(),
                entity.getUsername(),
                entity.getEmail()
        );
    }

    private User _getCore(Long entityId) {
        var opt_entity = repository.findById(entityId);

        return opt_entity.orElse(null);

    }

    @Override
    public List<UserReadDTO> getAll() {
        var allEntities = repository.findAll();
        return allEntities.stream().map((user) -> new UserReadDTO(
                user.getId(),
                user.getUsername(),
                user.getEmail()
        )).toList();
    }

    @Override
    public void update(Long entityId, UserCreateDTO newEntity) {
        var entityForm = new User();
        entityForm.setEmail(newEntity.email());
        entityForm.setPassword(newEntity.password());
        entityForm.setUsername(newEntity.username());

        repository.save(entityForm);
    }

    @Override
    public void delete(Long entityId) {
        var matchingEntity = _getCore(entityId);
        repository.delete(matchingEntity);
    }
}
