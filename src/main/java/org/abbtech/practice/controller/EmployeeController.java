package org.abbtech.practice.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.EmployeeDTO;
import org.abbtech.practice.dto.XUserGroup;
import org.abbtech.practice.dto.YUserGroup;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
@Validated
public class EmployeeController {

    @PostMapping
    public void saveEmployee(@RequestBody @Validated(value = YUserGroup.class) EmployeeDTO employee) {
    }

    @PostMapping("/custom")
    public void saveEmployeeValidated(@RequestBody @Validated(value = XUserGroup.class) EmployeeDTO employee) {
    }

    @GetMapping
    public List<EmployeeDTO> getAllEmployees() {
        return null;
    }
}
