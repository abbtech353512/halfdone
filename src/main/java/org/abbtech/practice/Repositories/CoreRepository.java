package org.abbtech.practice.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CoreRepository<TEntity> extends JpaRepository<TEntity, Long>{
}
